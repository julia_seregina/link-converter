import sqlalchemy

metadata = sqlalchemy.MetaData()

tbl = sqlalchemy.Table('links', metadata,
                       sqlalchemy.Column('id', sqlalchemy.Integer, primary_key=True, autoincrement=True),
                       sqlalchemy.Column('old_link', sqlalchemy.String(255)),
                       sqlalchemy.Column('new_link', sqlalchemy.String(255)),
                       sqlalchemy.Column('user', sqlalchemy.String(255))
                       )
