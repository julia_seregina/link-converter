from aiohttp import web
import random
import jinja2
import aiohttp_jinja2
from database import insert_link, select_link


@aiohttp_jinja2.template('index.html')
async def main_url(request):
    return {}


async def save_links(request):
    data = await request.post()
    old_link = data['long_url']
    new_link = ''.join(random.choice('0123456789abcdefghijklmnopqrstuvwxyz') for _ in range(6))
    await insert_link(old_link, new_link)
    return web.Response(text=f'http://127.0.0.1:8080/{new_link}')


async def redirect_url(request):
    new_link = request.match_info['new_link']
    old_link = await select_link(new_link)
    raise web.HTTPFound(old_link)

if __name__ == '__main__':
    app = web.Application()
    aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader("templates"))
    app.add_routes([web.get('/', main_url)])
    app.add_routes([web.post('/', save_links)])
    app.add_routes([web.get('/{new_link}', redirect_url)])
    web.run_app(app, host='0.0.0.0', port=8080)
