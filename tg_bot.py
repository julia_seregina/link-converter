import os

import random
import logging
from aiogram import Bot, Dispatcher, executor, types
from database import insert_link, get_user_links


API_TOKEN = os.getenv('API_TOKEN')
logging.basicConfig(level=logging.INFO)
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)


@dp.message_handler(commands=['start', 'help'])
async def send_welcome(message: types.Message):
    await message.reply("Hi!\nI'm Link converter Bot!\nSend me link")


@dp.message_handler(commands=['my_links'])
async def get_links(message: types.Message):
    user_links = await get_user_links(message.from_user.id)
    await message.reply(''.join(str(user_links)))


@dp.message_handler()
async def save_user_link(message: types.Message):
    old_link = message.text
    user = message.from_user.id
    if old_link.startswith('http') or old_link.startswith('https'):
        new_link = ''.join(random.choice('0123456789abcdefghijklmnopqrstuvwxyz') for _ in range(6))
        await insert_link(old_link, new_link, user)
        await message.answer(f'http://127.0.0.1:8080/{new_link}')
    else:
        await message.answer(f'Sorry, not correct link')


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)