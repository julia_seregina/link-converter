import os

from aiopg.sa import create_engine
from models import tbl


async def init_db():
    engine = await create_engine(database='postgres',
                                 user='postgres',
                                 password='postgres',
                                 host=os.getenv('POSTGRES_HOST', '127.0.0.1'),
                                 port=5432)
    return engine


async def insert_link(old_link, new_link, user=None):
    engine = await init_db()
    async with engine.acquire() as connection:
        await connection.execute(tbl.insert().values(old_link=old_link, new_link=new_link, user=user))


async def select_link(new_link):
    engine = await init_db()
    async with engine.acquire() as connection:
        result = await connection.execute(tbl.select().where(tbl.c.new_link == new_link))
        result = await result.fetchone()
        return result[1]


async def get_user_links(user):
    engine = await init_db()
    async with engine.acquire() as connection:
        results = await connection.execute(tbl.select().where(tbl.c.user == str(user)))
        results = await results.fetchall()
        links = [(result[1], f'http://127.0.0.1:8080/{result[2]}') for result in results]
        return links
